//
//  MemberView.swift
//  testapp
//
//  Created by Jiorgio Davis on 12/7/19.
//  Copyright © 2019 Jiorgio. All rights reserved.
//

import Foundation
import Combine
import SwiftUI

struct MemberView: View {
    @ObservedObject var memberViewModel: MemberViewModel
    @ObservedObject var viewRouter: ViewRouter
    @State private var searchText = ""
    @State private var showCancelButton: Bool = false
    
    init(viewRouter: ViewRouter) {
        self.viewRouter = viewRouter
        self.memberViewModel = MemberViewModel(viewRouter)
    }
    
    func currentSort() -> String {
        return self.memberViewModel.sortBy.capitalizingFirstLetter()
    }
    
    var body: some View {
        NavigationView {
              VStack(alignment: .leading) {
                HStack {
                    HStack {
                        Image(systemName: "magnifyingglass")
                        TextField("search", text: $searchText, onEditingChanged: { isEditing in
                            self.showCancelButton = true
                        }, onCommit: {
                            self.memberViewModel.search(self.searchText)
                        }).foregroundColor(.primary)

                        Button(action: {
                            self.searchText = ""
                            self.memberViewModel.defaultSort()
                        }) {
                            Image(systemName: "xmark.circle.fill").opacity(searchText == "" ? 0 : 1)
                        }
                    }
                    .padding(EdgeInsets(top: 8, leading: 10, bottom: 8, trailing: 10))
                    .foregroundColor(.secondary)
                    .background(Color(.secondarySystemBackground))
                    .cornerRadius(10.0)

                    if showCancelButton  {
                        Button("Cancel") {
                            self.searchText = ""
                            self.showCancelButton = false
                            self.memberViewModel.defaultSort()
                        }
                        .foregroundColor(Color(.systemBlue))
                    }
                }.padding(10)
                
                MemberList(viewModel: memberViewModel, viewRouter: viewRouter)
               
            }.navigationBarTitle("Members")
                .navigationBarItems(leading: HStack {
                    Image(systemName: "arrow.left").foregroundColor(.blue).onTapGesture {
                        self.viewRouter.page = "company"
                    }
                    Text("Sort By \(currentSort())")
                .foregroundColor(.blue)
                .contextMenu {
                    Button(action: {
                        self.memberViewModel.sortBy = "name"
                    }) {
                        Text("Name")
                        if self.memberViewModel.sortBy == "name" {
                             Image(uiImage: UIImage(named: "check-icon")!)
                             .resizable()
                              .aspectRatio(contentMode: .fit)
                            .frame(width: 8.0, height: 8.0)
                        }
                    }.onTapGesture {
                        
                    }
                Button(action: {
                    self.memberViewModel.sortBy = "age"
                }) {
                    Text("Age")
                    if self.memberViewModel.sortBy == "age" {
                         Image(uiImage: UIImage(named: "check-icon")!)
                         .resizable()
                          .aspectRatio(contentMode: .fit)
                        .frame(width: 10.0, height: 10.0)
                    }
                }.onTapGesture {
                    
                }
                    }
            }, trailing:
                Button(action: {
                    
                    if self.memberViewModel.sortIndex == 0 {
                        self.memberViewModel.sortOrder = "asc"
                        self.memberViewModel.sortAsc()
                    } else if self.memberViewModel.sortIndex == 1 {
                        self.memberViewModel.sortOrder = "desc"
                        self.memberViewModel.sortDesc()
                    } else if self.memberViewModel.sortIndex >= 2 {
                        self.memberViewModel.sortOrder = ""
                        self.memberViewModel.sortIndex = -1
                        self.memberViewModel.defaultSort()
                    }
                    
                    self.memberViewModel.sortIndex += 1
                }, label: {
                    
                    if self.memberViewModel.sortOrder == "" {
                        Image(uiImage: UIImage(named: "sort-by")!)
                        .resizable()
                        .frame(width: 24, height: 24, alignment: .trailing)
                    } else if self.memberViewModel.sortOrder == "asc" {
                        Image(uiImage: UIImage(named: "sort-asc")!)
                           .resizable()
                           .frame(width: 24, height: 24, alignment: .trailing)
                    } else if self.memberViewModel.sortOrder == "desc" {
                        Image(uiImage: UIImage(named: "sort-desc")!)
                        .resizable()
                        .frame(width: 24, height: 24, alignment: .trailing)
                    }

                }))
        }
    }
}

