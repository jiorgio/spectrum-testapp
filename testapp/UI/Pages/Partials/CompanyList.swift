//
//  CompanyList.swift
//  testapp
//
//  Created by Jiorgio Davis on 12/7/19.
//  Copyright © 2019 Jiorgio. All rights reserved.
//
import SwiftUI
import Combine

struct CompanyList: View {
    @ObservedObject var viewModel: CompanyViewModel
    @ObservedObject var viewRouter: ViewRouter
    
    func imageWhenAddedToFavorite(_ id: String) -> String {
        return self.viewModel.favoriteCompanies.keys.contains("\(id)-fav") ? "star.fill" : "star"
    }
    
    func bgWhenCompanyFollowed( _ id: String) -> Color {
        return self.viewModel.followedCompanies.keys.contains(id) ? Color.blue : Color.white
    }
    
    func borderWhenCompanyFollowed( _ id: String) -> Color {
        return self.viewModel.followedCompanies.keys.contains(id) ? Color.white : Color.blue
    }
    
    func fgWhenCompanyFollowed( _ id: String) -> Color {
        return self.viewModel.followedCompanies.keys.contains(id) ? .white : .blue
    }
    
    var body: some View {
        List(self.viewModel.companies) { company in
           HStack(alignment: .center) {
               ImageView(withURL: company.logo)
               VStack(alignment: .leading) {
                   Text(company.company).font(.system(size: 20))
                   Text(company.website)
                       .foregroundColor(.blue)
                       .font(.system(size: 14))
                       .onTapGesture {
                           if let url = URL(string: "http://\(company.website)"), !url.absoluteString.isEmpty {
                               UIApplication.shared.open(url, options: [:], completionHandler: nil)
                           }
                       }
                Text(company.about).font(.system(size: 10)).lineLimit(nil).padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 60))
               }.frame(width: 180, height: 30, alignment: .leading)
               
               VStack(alignment: .trailing) {
                   HStack(alignment: .center) {
                        Button(action: {}) {
                           Text("Follow")
                           .foregroundColor(self.fgWhenCompanyFollowed(company.id))
                        }
                        .frame(width: 60, height: 30, alignment: .center)
                        .background(self.bgWhenCompanyFollowed(company.id))
                        .border(self.borderWhenCompanyFollowed(company.id), width: 1)
                        .padding()
                        .onTapGesture {
                            self.viewModel.followCompany(company.id, company.company)
                        }
                    
                        Button(action: {}) {
                            Image(systemName: self.imageWhenAddedToFavorite(company.id))
                        .foregroundColor(.blue)
                        .padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 10))
                            .onTapGesture {
                                self.viewModel.favoriteCompany(company.id, company.company)
                            }
                        }
                   }.frame(width: 130, height: 50, alignment: .trailing)
               }.frame(width: 80, height: 30, alignment: .trailing)
           }
           .padding(EdgeInsets(top: 20, leading: 10, bottom: 20, trailing: 10))
           .onTapGesture {
               CompanyViewModel.currentMemberData = company.members
               self.viewRouter.page = "member"
           }
       }
    }
}
