//
//  MemberList.swift
//  testapp
//
//  Created by Jiorgio Davis on 12/7/19.
//  Copyright © 2019 Jiorgio. All rights reserved.
//
import SwiftUI
import Combine

struct MemberList: View {
    @ObservedObject var viewModel: MemberViewModel
    @ObservedObject var viewRouter: ViewRouter
    
    func bgWhenCompanyFollowed( _ id: String) -> Color {
        return self.viewModel.followedMembers.keys.contains(id) ? Color.blue : Color.white
    }
    
    func borderWhenCompanyFollowed( _ id: String) -> Color {
        return self.viewModel.followedMembers.keys.contains(id) ? Color.white : Color.blue
    }
    
    func fgWhenCompanyFollowed( _ id: String) -> Color {
        return self.viewModel.followedMembers.keys.contains(id) ? .white : .blue
    }
    
    var body: some View {
        List(self.viewModel.members) { member in
           HStack(alignment: .center) {
               VStack(alignment: .leading) {
                Text("\(String(member.name["first"]!)) \(String(member.name["last"]!))").font(.system(size: 20))
                Text("\(member.email)").font(.system(size: 14))
                Text("\(member.phone)").font(.system(size: 12))
                Text("age: \(member.age)").font(.system(size: 12))
            
               }.frame(width: 250, height: 30, alignment: .leading)
               
               VStack(alignment: .trailing) {
                   HStack(alignment: .center) {
                        Button(action: {}) {
                           Text("Follow")
                           .foregroundColor(self.fgWhenCompanyFollowed(member.id))
                        }
                        .frame(width: 60, height: 30, alignment: .center)
                        .background(self.bgWhenCompanyFollowed(member.id))
                        .border(self.borderWhenCompanyFollowed(member.id), width: 1)
                        .padding()
                        .onTapGesture {
                            self.viewModel.followMember(member.id, member.email)
                        }
                    
                   }.frame(width: 130, height: 50, alignment: .trailing)
               }.frame(width: 80, height: 30, alignment: .trailing)
           }
           .padding(EdgeInsets(top: 20, leading: 10, bottom: 20, trailing: 10))
           .onTapGesture {
            print(member.email)
               self.viewRouter.page = "member"
           }
       }
    }
}
