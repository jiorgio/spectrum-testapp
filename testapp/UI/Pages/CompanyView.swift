//
//  CompanyView.swift
//  testapp
//
//  Created by Jiorgio Davis on 12/7/19.
//  Copyright © 2019 Jiorgio. All rights reserved.
//

import Foundation
import Combine
import SwiftUI

struct CompanyView: View {
    @ObservedObject var companyViewModel: CompanyViewModel
    @ObservedObject var viewRouter: ViewRouter
    @State private var searchText = ""
    @State private var showCancelButton: Bool = false
    
    init(viewRouter: ViewRouter) {
        self.viewRouter = viewRouter
        self.companyViewModel = CompanyViewModel(viewRouter)
    }
    
    var body: some View {
        NavigationView {
             VStack(alignment: .leading) {
               HStack {
                   HStack {
                       Image(systemName: "magnifyingglass")
                       TextField("search", text: $searchText, onEditingChanged: { isEditing in
                           self.showCancelButton = true
                       }, onCommit: {
                           self.companyViewModel.search(self.searchText)
                       }).foregroundColor(.primary)

                       Button(action: {
                           self.searchText = ""
                           self.companyViewModel.defaultSort()
                       }) {
                           Image(systemName: "xmark.circle.fill").opacity(searchText == "" ? 0 : 1)
                       }
                   }
                   .padding(EdgeInsets(top: 8, leading: 10, bottom: 8, trailing: 10))
                   .foregroundColor(.secondary)
                   .background(Color(.secondarySystemBackground))
                   .cornerRadius(10.0)

                   if showCancelButton  {
                       Button("Cancel") {
                           self.searchText = ""
                           self.showCancelButton = false
                           self.companyViewModel.defaultSort()
                       }
                       .foregroundColor(Color(.systemBlue))
                   }
               }.padding(10)
               
               CompanyList(viewModel: companyViewModel, viewRouter: viewRouter)
              
           }.navigationBarTitle("Company")
           .navigationBarItems(trailing:
               Button(action: {
                   
                   if self.companyViewModel.sortIndex == 0 {
                       self.companyViewModel.sortOrder = "asc"
                       self.companyViewModel.sortAsc()
                   } else if self.companyViewModel.sortIndex == 1 {
                       self.companyViewModel.sortOrder = "desc"
                       self.companyViewModel.sortDesc()
                   } else if self.companyViewModel.sortIndex >= 2 {
                       self.companyViewModel.sortOrder = ""
                       self.companyViewModel.sortIndex = -1
                       self.companyViewModel.defaultSort()
                   }
                   
                   self.companyViewModel.sortIndex += 1
               }, label: {
                   
                   if self.companyViewModel.sortOrder == "" {
                       Image(uiImage: UIImage(named: "sort-by")!)
                       .resizable()
                       .frame(width: 24, height: 24, alignment: .trailing)
                   } else if self.companyViewModel.sortOrder == "asc" {
                       Image(uiImage: UIImage(named: "sort-asc")!)
                          .resizable()
                          .frame(width: 24, height: 24, alignment: .trailing)
                   } else if self.companyViewModel.sortOrder == "desc" {
                       Image(uiImage: UIImage(named: "sort-desc")!)
                       .resizable()
                       .frame(width: 24, height: 24, alignment: .trailing)
                   }

               }))
        }
    }
}
