//
//  ContentView.swift
//  testapp
//
//  Created by Jiorgio Davis on 12/7/19.
//  Copyright © 2019 Jiorgio. All rights reserved.
//

import SwiftUI

struct Route: View {
    @State var page = "company"
    @ObservedObject var viewRouter: ViewRouter
    
    func PageViews() -> AnyView {
        switch self.viewRouter.page {
            case "company": return AnyView(CompanyView(viewRouter: viewRouter))
            case "member": return AnyView(MemberView(viewRouter: viewRouter))
        default:
            return AnyView(CompanyView(viewRouter: viewRouter))
        }
    }
    
    var body: some View {
        VStack {
            self.PageViews()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        Route(viewRouter: ViewRouter())
    }
}
