//
//  ViewRouter.swift
//  testapp
//
//  Created by Jiorgio Davis on 12/7/19.
//  Copyright © 2019 Jiorgio. All rights reserved.
//

import Foundation
import SwiftUI
import Combine

class ViewRouter: ObservableObject {
    let objectWillChange = PassthroughSubject<ViewRouter, Never>()
    
    var page: String = "company" {
        didSet {
            objectWillChange.send(self)
        }
    }
    
    func set(_ page: String) {
        self.page = page
    }
}
