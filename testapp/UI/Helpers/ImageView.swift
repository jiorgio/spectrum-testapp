//
//  ImageView.swift
//  testapp
//
//  Created by Jiorgio Davis on 12/7/19.
//  Copyright © 2019 Jiorgio. All rights reserved.
//
import Combine
import SwiftUI

struct ImageView: View {
    @ObservedObject var imageLoader: ImageLoader

    init(withURL imageUrl: String) {
         imageLoader = ImageLoader(urlString: imageUrl)
    }

    var body: some View {
        Image(uiImage: UIImage(data: imageLoader.data) ?? UIImage())
            .resizable()
            .aspectRatio(contentMode: .fill)
            .clipShape(Circle())
            .frame(width: 70.0, height: 70.0)
    }
}
