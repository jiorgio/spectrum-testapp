//
//  MemberViewModel.swift
//  testapp
//
//  Created by Jiorgio Davis on 12/7/19.
//  Copyright © 2019 Jiorgio. All rights reserved.
//

import SwiftUI
import Combine
import SQLite

class MemberViewModel: ObservableObject {
    @Published var members: [Member] = []
    @Published var defaultData: [Member] = []
    @Published var searchResult: [Member] = []
    @Published var viewRouter: ViewRouter?
    @Published var followedMembers = [String: String]()
    @Published var sortOrder = ""
    @Published var sortBy = "name"
    @Published var sortIndex = 0

    init(_ viewRouter: ViewRouter) {
        self.viewRouter = viewRouter
        self.loadData()
        self.fetchMembers()
    }
    
    func loadData() {
        let sqlite = SqliteHelper.shared
        let table = sqlite.table("favourites")
        do {
           
            for favorite in try (sqlite.db!.prepare(table)) {
                let id = (try favorite.get(sqlite.field("id")))
                let type = (try favorite.get(sqlite.field("type")))
                let company = (try favorite.get(sqlite.field("company")))
                
                if type == "follow" {
                    self.followedMembers[id] = company
                }
            }

        } catch {
            print(error)
        }
        
    }
    
    func sortAsc() {
        if self.sortBy == "name" {
           self.members = self.members.sorted(by: { "\(String($0.name["first"]!)) \(String($0.name["last"]!))" < "\(String($1.name["first"]!)) \(String($1.name["last"]!))" })
        } else if self.sortBy == "age" {
            self.members = self.members.sorted(by: { $0.age < $1.age })
        }
    }
    
    func sortDesc() {
        if self.sortBy == "name" {
           self.members = self.members.sorted(by: { "\(String($0.name["first"]!)) \(String($0.name["last"]!))" > "\(String($1.name["first"]!)) \(String($1.name["last"]!))" })
        } else if self.sortBy == "age" {
            self.members = self.members.sorted(by: { $0.age > $1.age })
        }
    }
    
    func followMember(_ id: String, _ email: String) {
        if followedMembers.keys.contains(id) {
            followedMembers.removeValue(forKey: id)
            SqliteHelper.shared.delete("favourites", id)
        } else {
            followedMembers[id] = email
            
            let favorite = Favorite(id: id, type: "follow", company: email)
            
            if SqliteHelper.shared.insert("favourites", favorite) {
                print("success")
            }
        }
    }
    
    func defaultSort() {
        self.members = self.defaultData
    }
    
    func search(_ filter: String) {
        if filter.isEmpty {
            self.members = self.defaultData
            return
        }
        
        self.members = self.members.filter { "\(String($0.name["first"]!)) \(String($0.name["last"]!))".lowercased().contains(filter.lowercased()) }
        print(self.members)
    }
    
    func fetchMembers() {
        self.members = CompanyViewModel.currentMemberData!
        self.defaultData = self.members
    }
}
