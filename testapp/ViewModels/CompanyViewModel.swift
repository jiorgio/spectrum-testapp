//
//  CompanyViewModel.swift
//  testapp
//
//  Created by Jiorgio Davis on 12/7/19.
//  Copyright © 2019 Jiorgio. All rights reserved.
//


import SwiftUI
import Combine
import SQLite

class CompanyViewModel: ObservableObject {
    @Published var companies: [Company] = []
    @Published var defaultData: [Company] = []
    @Published var searchResult: [Company] = []
    @Published var viewRouter: ViewRouter?
    @Published var followedCompanies = [String: String]()
    @Published var favoriteCompanies = [String: String]()
    @Published var sortOrder = ""
    @Published var sortIndex = 0
    static var currentMemberData: [Member]?
    
    init(_ viewRouter: ViewRouter) {
        self.viewRouter = viewRouter
        self.loadData()
        self.fetchCompanies()
    }
    
    func loadData() {
        let sqlite = SqliteHelper.shared
        let table = sqlite.table("favourites")
        do {
           
            for favorite in try (sqlite.db!.prepare(table)) {
                let id = (try favorite.get(sqlite.field("id")))
                let type = (try favorite.get(sqlite.field("type")))
                let company = (try favorite.get(sqlite.field("company")))
                
                if type == "follow" {
                    self.followedCompanies[id] = company
                } else if type == "favorite" {
                    self.favoriteCompanies[id] = company
                }
            }

        } catch {
            print(error)
        }
    }
    
    func sortAsc() {
        self.companies = self.companies.sorted(by: { $0.company < $1.company })
    }
    
    func sortDesc() {
        self.companies = self.companies.sorted(by: { $0.company > $1.company })
    }
    
    func followCompany(_ id: String, _ company: String) {
        if followedCompanies.keys.contains(id) {
            followedCompanies.removeValue(forKey: id)
            SqliteHelper.shared.delete("favourites", id)
        } else {
            followedCompanies[id] = company
            
            let favorite = Favorite(id: id, type: "follow", company: company)
            
            if SqliteHelper.shared.insert("favourites", favorite) {
                print("success")
            }
        }
    }
    
    func favoriteCompany(_ id: String, _ company: String) {
        let uid = "\(id)-fav"
        if favoriteCompanies.keys.contains(uid) {
            favoriteCompanies.removeValue(forKey: uid)
            SqliteHelper.shared.delete("favourites", uid)
        } else {
            favoriteCompanies[uid] = company
            
            let favorite = Favorite(id: uid, type: "favorite", company: company)
                       
            if SqliteHelper.shared.insert("favourites", favorite) {
               print("success")
            }
        }
    }
    
    func defaultSort() {
        self.companies = self.defaultData
    }
    
    func search(_ filter: String) {
        if filter.isEmpty {
            self.companies = self.defaultData
            return
        }
        
        self.companies = self.companies.filter { $0.company.lowercased().contains(filter.lowercased()) }
        print(self.companies)
    }
    
    func fetchCompanies() {
        guard let url = URL(string: Constants.ApiURL) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            DispatchQueue.main.async {
                do {
                    self.companies = try JSONDecoder().decode([Company].self, from: data!)
                    self.defaultData = self.companies
                } catch {
                    print("failed", error)
                }
            }
        }.resume()
    }
}
