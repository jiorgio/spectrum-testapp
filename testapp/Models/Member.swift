//
//  Member.swift
//  testapp
//
//  Created by Jiorgio Davis on 12/7/19.
//  Copyright © 2019 Jiorgio. All rights reserved.
//

struct Member: Identifiable, Codable {
    var id: String
    var age: Int
    var email: String
    var name: [String: String]
    var phone: String
    var fullname: String?
    static var shared: Member?
    
    private enum CodingKeys: String, CodingKey {
        case id = "_id"
        case age, email, name, phone
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(String.self, forKey: .id)
        age = try container.decode(Int.self, forKey: .age)
        email = try container.decode(String.self, forKey: .email)
        phone = try container.decode(String.self, forKey: .phone)
        name = try container.decode(Dictionary.self, forKey: .name)
        fullname = "\(String(name["first"]!)) \(String(name["last"]!))"
        
        Member.shared = self
    }
    
}
