//
//  Favorite.swift
//  testapp
//
//  Created by Jiorgio Davis on 12/7/19.
//  Copyright © 2019 Jiorgio. All rights reserved.
//

struct Favorite: Encodable {
    var id: String
    var type: String
    var company: String
}
