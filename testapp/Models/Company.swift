//
//  Company.swift
//  testapp
//
//  Created by Jiorgio Davis on 12/7/19.
//  Copyright © 2019 Jiorgio. All rights reserved.
//

struct Company: Identifiable, Codable {
    var id: String
    var company: String
    var website: String
    var logo: String
    var about: String
    var members: [Member]
    static var shared: Company?
    
    private enum CodingKeys: String, CodingKey {
          case id = "_id"
          case company
          case website
          case logo
          case about
          case members
    }
    
    init(from decoder: Decoder) throws {
       let container = try decoder.container(keyedBy: CodingKeys.self)
            
        id = try container.decode(String.self, forKey: .id)
        company = try container.decode(String.self, forKey: .company)
        website = try container.decode(String.self, forKey: .website)
        logo = try container.decode(String.self, forKey: .logo)
        about = (try container.decodeIfPresent(String.self, forKey: .about)) ?? ""
        members = try container.decodeIfPresent([Member].self, forKey: .members) ?? []
        
        Company.shared = self
    }
     
}
