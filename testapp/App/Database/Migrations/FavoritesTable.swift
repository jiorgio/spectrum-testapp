//
//  FavoritesTable.swift
//  testapp
//
//  Created by Jiorgio Davis on 12/7/19.
//  Copyright © 2019 Jiorgio. All rights reserved.
//

import SQLite

class FavoritesTable {
    static let shared = FavoritesTable()
    let table = Table("favourites")
    
    func up() -> String {
        return self.table.create { t in
            t.column(Expression<String>("id"), primaryKey: true)
            t.column(Expression<String>("type"))
            t.column(Expression<String>("company"))
        }
    }
    
    func down() {
        _ = self.table.drop(ifExists: true)
    }
}
