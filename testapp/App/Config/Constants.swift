//
//  Constants.swift
//  testapp
//
//  Created by Jiorgio Davis on 12/7/19.
//  Copyright © 2019 Jiorgio. All rights reserved.
//

struct Constants {
    static var ApiURL: String = "https://next.json-generator.com/api/json/get/Vk-LhK44U"
    static var SqliteDbName: String = "TestApp"
}
