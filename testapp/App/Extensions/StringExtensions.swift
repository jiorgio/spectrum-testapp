//
//  StringExtensions.swift
//  testapp
//
//  Created by Jiorgio Davis on 12/7/19.
//  Copyright © 2019 Jiorgio. All rights reserved.
//

extension String {
    func capitalizingFirstLetter() -> String {
      return prefix(1).uppercased() + self.lowercased().dropFirst()
    }

    mutating func capitalizeFirstLetter() {
      self = self.capitalizingFirstLetter()
    }
}
