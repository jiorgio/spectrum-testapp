//
//  SqliteHelper.swift
//  testapp
//
//  Created by Jiorgio Davis on 12/7/19.
//  Copyright © 2019 Jiorgio. All rights reserved.
//
import SQLite

class SqliteHelper {
    var db: Connection?
    var currentTable: Table?
    var version: Int = 1
    static var shared: SqliteHelper = SqliteHelper()

    init() {
       do {
           let baseFolderPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
           self.db = try Connection("\(baseFolderPath)/\(Constants.SqliteDbName).sqlite3")
           self.create()
       } catch {
           print("SqliteHelperClass: failed to create database")
       }
    }

    func all(_ table: String) -> Any {
       do {
           return try self.db!.prepare(Table(table))
       } catch {
           print("Failed to fetch all records")
       }
       
       return false
    }

    func table(_ table: String) -> Table {
       return Table(table)
    }

    func find(_ paramsId: Int64) -> Table {
       let id = Expression<Int64>("id")
       return self.currentTable!.filter(id == paramsId)
    }

    func field(_ id: String) -> Expression<String> {
      return Expression<String>(id)
    }

    func insert(_ table: String, _ params: Encodable) -> Bool {
       self.currentTable = Table(table)
       do {
           try self.db?.run((self.currentTable?.insert(params))!)
           return true
       } catch {
           print(error)
           return false
       }
    }

    func delete( _ table: String, _ id: String) {
       let table = Table(table)
       let resource = table.filter(Expression<String>("id") == id)
       do {
           try self.db?.run(resource.delete())
       } catch {
           print(error)
           print("failed to delete: \(id)")
       }
    }

    func create() {
       do {
           try self.db!.run(FavoritesTable.shared.up())
       } catch {
           print("SqliteHelper ~> failed to run create table")
       }
    }
}
